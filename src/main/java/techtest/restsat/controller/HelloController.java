package techtest.restsat.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	private static Logger logger = LoggerFactory.getLogger(HelloController.class);

	@GetMapping("/hello")
	public String hello(@RequestParam(defaultValue = "World") String name) {

		logger.info("Hello {} from the REST Server!", name);

		return "Hello " + name + " from the REST Server!";
	}

}
